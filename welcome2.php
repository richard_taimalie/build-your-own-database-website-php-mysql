<?php
    $name = $_GET['name'];
?>

<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Turn link to plain text to stop Security leak</title>
</head>

<body>
   
    <?php
        echo 'Welcome to out website, ' . htmlspecialchars($name, ENT_QUOTES, 'UTF-8') . '!';
    ?>
    
</body>
</html>

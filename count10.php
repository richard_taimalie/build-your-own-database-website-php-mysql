<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>While Loop</title>
</head>

<body>
    
    
    <?php
    $count = 1;
    while ($count <= 10)
    {
        echo "$count ";
        ++$count;
    }
    ?>
    
</body>
</html>

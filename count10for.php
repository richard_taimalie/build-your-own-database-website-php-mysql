<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>While Loop</title>
</head>

<body>
    
    
    <?php
        for ($count = 1; $count <= 10; ++$count)
        {
            echo "$count ";
        }
    ?>
    
</body>
</html>

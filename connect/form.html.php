<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Joke</title>
    <style type="text/css">
        textarea    {
            display: block;
            width: 100%;
        }
    </style>
</head>

<body>
    
    <form action="?" method="post">
        <div>
            <label for="joketext"> Type your joke here:</label>
            <textarea id="joketext" name="joketext" rows="3" cols="40"></textarea>
        </div>
        <div><input type="submit" value="Add" /> </div>
    </form>
    
</body>
</html>
